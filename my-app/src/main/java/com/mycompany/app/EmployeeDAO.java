import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAO {
	static private List<EmployeeBean> lo=new ArrayList<>();
	static private String line;
	static private BufferedReader br;
	/*static {
		try {
			br = new BufferedReader(new FileReader("C:\\Users\\sudagraw\\Documents\\EmployeeDetails.csv"));
			lo = new ArrayList<>();
			while((line=br.readLine())!=null) {
				String[] details = line.split(",");
				EmployeeBean employee = new EmployeeBean(details[1],Integer.parseInt(details[0]),Float.parseFloat(details[2]));
				lo.add(employee);
				}
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}*/
	public List<EmployeeBean> readData(){
		return lo;
}
	public float getTotSal(List<EmployeeBean> lo) {
		float totSal = (float)lo.stream().mapToDouble(e->e.getSalary()).sum();
		return totSal;
	}
	public int getCount(float salary,List<EmployeeBean> lo) {
		int count= (int)lo.stream().filter(e->e.getSalary()==salary).count();
		return count;
	}
	public EmployeeBean getEmployee(int id) throws UserNotFoundException{
		EmployeeBean employee = null;
		for(EmployeeBean e : lo) {
			if(e.getID()==id)
			{	employee = e;
			break;}
		}
		if(employee == null)
		{
			throw new UserNotFoundException();
		}
		return employee;
	}
}
